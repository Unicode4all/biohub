package cmd

import (
	"database/sql"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"

	"fmt"
	"os"
	"strconv"

	"github.com/Unicode4all/biohub/data"
	"github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/lib/pq"
	"github.com/spf13/viper"
)

var forceMigrateCmd = &cobra.Command{
	Use:   "force-migrate [version]",
	Short: "Forcefully apply a database migration. Useful for fixing broken database updates.",
	Args:  cobra.MinimumNArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		db, err := sql.Open("postgres", viper.GetString("Database.ConnectionString"))
		if err != nil {
			fmt.Printf("Cannot connect to database: %v", err.Error())
			os.Exit(1)
		}
		driver, err := postgres.WithInstance(db, &postgres.Config{})
		if err != nil {
			logrus.Errorf("Failed to create PostgreSQL driver instance: %v", err.Error())
			return
		}

		mgn, err := strconv.ParseInt(args[0], 10, 32)

		m := int(mgn)

		if err != nil {
			fmt.Errorf("force-migrate requires an integer argument")
			os.Exit(1)
		}

		data.MigrateDB(db, driver, true, m)
	},
}
