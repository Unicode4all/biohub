package api

import (
	"encoding/json"
	"net/http"

	v1 "github.com/Unicode4all/biohub/api/v1"
	"github.com/Unicode4all/biohub/models"
	"github.com/go-chi/chi"
	"github.com/go-chi/render"
)

func NewAPI() http.Handler {
	router := chi.NewRouter()

	router.Use(render.SetContentType(render.ContentTypeJSON))
	router.Use(RecovererMiddleware)

	router.NotFound(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(404)

		js, _ := json.Marshal(models.ErrorDTO{
			Code:  404,
			Error: "Not found",
		})
		w.Write(js)
	})

	router.Mount("/v1", v1.NewAPI())

	return router
}
