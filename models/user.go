package models

import (
	"time"

	"github.com/mitchellh/mapstructure"
)

// User defines an application user entity
type User struct {
	ID                string    `json:"id,omitempty"`
	Username          string    `json:"username,omitempty"`
	Email             string    `json:"email,omitempty"`
	Password          string    `json:"password,omitempty"` // Password hash
	Locked            bool      `json:"locked,omitempty"`
	EmailConfirmed    bool      `json:"emailconfirmed"`
	TwoFactorEnabled  bool      `json:"twofactorenabled"`
	Role              string    `json:"role,omitempty"`
	Registered        time.Time `json:"registered,omitempty"`
	AccessFailedCount int       `json:"accessfailed"`
	Bans              []Ban     `json:"bans,omitempty"`
	Notices           []Notice  `json:"notices,omitempty"`
}

// Map creates a map[string]interface{} from User object
func (user *User) Map() map[string]interface{} {

	m := make(map[string]interface{})
	mapstructure.Decode(user, &m)

	return m
}

// NewUserFromMap creates a user object from a map. This is used for caching
func NewUserFromMap(umap map[string]string) *User {
	/*
		locked, err := strconv.ParseBool(umap["locked"])
		if err != nil {
			logrus.WithField("id", umap["id"]).Debug("Failed to convert from Locked string to bool")
		}

		registered, _ := time.Parse(time.RFC3339, umap["registered"]) // There can't be a error. Really.

		return &User{
			ID:         umap["id"],
			Username:   umap["username"],
			Email:      umap["email"],
			Password:   umap["password"],
			Locked:     locked,
			Registered: registered,
		}
	*/

	u := new(User)

	mapstructure.Decode(umap, u)

	return u
}
