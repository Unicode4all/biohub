package api

import (
	"net/http"

	"github.com/pkg/errors"

	"github.com/Unicode4all/biohub/helpers"
)

type stackTracer interface {
	StackTrace() errors.StackTrace
}

func RecovererMiddleware(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var err error
		rCopy := new(http.Request)
		*rCopy = *r
		defer func() {
			r := recover()
			if r != nil {
				switch t := r.(type) {
				case string:
					err = errors.New(t)
				case error:
					err = t
				default:
					err = errors.New("Unknown error")
				}

				helpers.APIError(w, rCopy, http.StatusInternalServerError, err)
			}
		}()
		h.ServeHTTP(w, r)
	})
}
