package server

import (
	"encoding/json"
	"net/http"

	"github.com/Unicode4all/biohub/api"
	"github.com/Unicode4all/biohub/models"
	umiddleware "github.com/Unicode4all/biohub/server/middleware"
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
)

func NewBaseRouter() http.Handler {
	baseRouter := chi.NewRouter()

	baseRouter.Use(middleware.RequestID)
	baseRouter.Use(middleware.RealIP)
	baseRouter.Use(middleware.Recoverer)
	baseRouter.Use(umiddleware.Logger)

	baseRouter.Get("/", func(w http.ResponseWriter, r *http.Request) {
		w.Write([]byte("Hello"))
	})

	baseRouter.NotFound(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(404)

		js, _ := json.Marshal(models.ErrorDTO{
			Code:  404,
			Error: "Not found",
		})
		w.Write(js)
	})

	baseRouter.Mount("/api", api.NewAPI())

	return baseRouter
}
