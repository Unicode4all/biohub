package data

import "github.com/Unicode4all/biohub/models"

type UserRepo interface {
	AddOrUpdate(newUser models.User) error
	FindById(id string) (*models.User, error)
	FindByEmail(email string) (*models.User, error)
	Find(query string) ([]*models.User, error)
}
