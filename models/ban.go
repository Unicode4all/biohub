package models

import "time"

type Ban struct {
	Id     string
	UserId string

	Date     time.Time
	Duration time.Duration

	Type string

	Jobs []string
}
