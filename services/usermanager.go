package services

import (
	"net/http"

	"github.com/Unicode4all/biohub/data"
	"github.com/Unicode4all/biohub/models"
	"github.com/Unicode4all/biohub/models/reqmodels"
	"github.com/Unicode4all/biohub/security"
	"github.com/Unicode4all/biohub/services/primitives"
	"github.com/pkg/errors"
	"golang.org/x/crypto/bcrypt"
)

// UserManager authenticates users, issues tokens and provides additional Security-related methods
type UserManager struct {
	userRepo data.UserRepo
}

func NewUserManager() (*UserManager, error) {
	um := new(UserManager)

	var userRepo data.UserRepo
	err := primitives.ServiceCollection.GetService(&userRepo)
	if err != nil {
		return nil, err
	}

	um.userRepo = userRepo
	return um, nil
}

func (userManager *UserManager) Register(user reqmodels.User) (string, error) {

	pass, e := hashAndSalt([]byte(user.Password))
	if e != nil {
		return "", e
	}

	u := models.User{
		Username:          user.Username,
		Email:             user.Email,
		Password:          pass,
		EmailConfirmed:    false,
		TwoFactorEnabled:  false,
		AccessFailedCount: 0,
		Role:              "user",
		Locked:            false,
	}

	e = userManager.userRepo.AddOrUpdate(u)
	if e != nil {
		return "", e
	}

	return "", nil
}

// SignIn checks credentials and issues an access token if authentication was successful
func (userManager *UserManager) SignIn(user reqmodels.Login, r *http.Request, w http.ResponseWriter) error {
	u, err := userManager.userRepo.FindByEmail(user.Email)
	if err != nil {
		return errors.Wrapf(err, "Usermanager: unable to sign in: %v", err.Error())
	}

	err = bcrypt.CompareHashAndPassword([]byte(u.Password), []byte(user.Password))
	if err != nil {
		return errors.Wrapf(err, "Login failed: %v", err.Error())
	}

	var sessionManager *security.SessionManager
	err = primitives.ServiceCollection.GetService(&sessionManager)
	if err != nil {
		panic(err)
	}

	session := sessionManager.StartSession(w, r)
	session.Set("username", u.Username)
	return nil
}

func hashAndSalt(pwd []byte) (string, error) {
	hash, err := bcrypt.GenerateFromPassword(pwd, bcrypt.DefaultCost)
	if err != nil {
		return "", err
	}

	return string(hash), nil
}
