package data

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"reflect"
	"time"

	"github.com/pkg/errors"

	"github.com/Unicode4all/biohub/cache"
	"github.com/Unicode4all/biohub/helpers/berrors"
	"github.com/Unicode4all/biohub/models"
	"github.com/Unicode4all/biohub/services/primitives"
	"github.com/go-redis/redis"
	"github.com/google/uuid"
	"github.com/lib/pq"
	"github.com/sirupsen/logrus"
)

// UserRepoSQL is an implementation of UserRepo that uses SQL
type UserRepoSQL struct {
	_db    *sql.DB
	_cache *cache.Cache

	addStmt      *sql.Stmt
	updateStmt   *sql.Stmt
	deleteStmt   *sql.Stmt
	findStmt     *sql.Stmt
	getStmt      *sql.Stmt
	getEmailStmt *sql.Stmt
}

// NewUserRepoSQL creates an instance for UserRepo interface that uses PostgreSQL and Redis
func NewUserRepoSQL(db *sql.DB, redis *redis.Client) (*UserRepoSQL, error) {
	repo := new(UserRepoSQL)

	repo._db = db

	err := primitives.ServiceCollection.GetService(&repo._cache)
	if err != nil {
		return nil, err
	}

	repo.addStmt, err = repo._db.Prepare("insert into Users values ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10);")
	if err != nil {
		return repo, err
	}

	repo.updateStmt, err = repo._db.Prepare(`
		UPDATE Users
		SET Username = $1,
			Password = $2,
			Email = $3,
			Locked = $4,
			EmailConfirmed = $5,
			AccessFailedCount = $6,
			Role = $7
		WHERE Id = $8;
	`)

	if err != nil {
		return repo, err
	}

	repo.deleteStmt, err = repo._db.Prepare("DELETE FROM Users WHERE Id = $1")
	if err != nil {
		return repo, err
	}
	repo.findStmt, err = repo._db.Prepare(`SELECT *
										  FROM Users
										  WHERE Username LIKE '%$1%' OR
												Email	 LIKE '%$1%'; 
										`)

	if err != nil {
		return repo, err
	}
	repo.getStmt, err = repo._db.Prepare("SELECT * FROM Users WHERE Id = $1;")
	if err != nil {
		return repo, err
	}

	repo.getEmailStmt, err = repo._db.Prepare("SELECT * FROM Users WHERE Email = $1;")
	if err != nil {
		return repo, err
	}
	return repo, nil
}

// AddOrUpdate attemts to add a user to the database and updates it if it exists.
// Will update if User ID is supplied
func (repo *UserRepoSQL) AddOrUpdate(user models.User) error {
	if user.ID != "" {
		logrus.WithFields(logrus.Fields{
			"object": reflect.TypeOf(repo).String(),
			"method": "AddOrUpdate",
		}).Debug("Updating user")
		_, err := repo.updateStmt.Exec(user.Username, user.Password, user.Email, user.Locked, user.ID)
		if err != nil {
			logrus.WithFields(logrus.Fields{
				"object": reflect.TypeOf(repo).String(),
				"method": "AddOrUpdate",
			}).Debug("Error updating user")
			logrus.WithFields(logrus.Fields{
				"error": err.Error(),
			}).Error("Cannot update User")
		}

		// Invalidate cache after update
		logrus.WithFields(logrus.Fields{
			"object": reflect.TypeOf(repo).String(),
			"method": "AddOrUpdate",
		}).Debug("Invalidating cache")
		repo.invalidateCache(user.ID)
		return nil
	}

	user.ID = uuid.New().String()
	user.Registered = time.Now()

	logrus.WithFields(logrus.Fields{
		"object": reflect.TypeOf(repo).String(),
		"method": "AddOrUpdate",
	}).Debug("Adding user")

	_, err := repo.addStmt.Exec(user.ID,
		user.Username,
		user.Password,
		user.Email,
		user.EmailConfirmed,
		user.Role,
		user.Locked,
		user.TwoFactorEnabled,
		user.Registered,
		user.AccessFailedCount)

	if err != nil {

		if pgerr, ok := err.(*pq.Error); ok {
			if pgerr.Code == "23505" {
				var e error
				switch pgerr.Constraint {
				case "users_username_key":
					e = berrors.NewBioDuplicateEntry("username")
					break
				case "users_email_key":
					e = berrors.NewBioDuplicateEntry("email")
					break
				default:
					e = berrors.NewBioDuplicateEntry("unknown")
				}
				return e
			}
		}

		logrus.WithFields(logrus.Fields{
			"object": reflect.TypeOf(repo).String(),
			"method": "AddOrUpdate",
		}).Debug("Error adding user")
		return err

	}

	logrus.WithFields(logrus.Fields{
		"object": reflect.TypeOf(repo).String(),
		"method": "AddOrUpdate",
	}).Debug("Caching user")
	repo.cache(user)
	return nil
}

// FindById tries to retrieve a User from database
func (repo *UserRepoSQL) FindById(id string) (*models.User, error) {
	logrus.WithFields(logrus.Fields{
		"object": reflect.TypeOf(repo).String(),
		"method": "FindById",
		"id":     id,
	}).Debug("Retrieving user from cache")
	u, err := repo.retrieveFromCache(id)
	if err == redis.Nil || err == berrors.BioCacheDisabled {
		logrus.WithFields(logrus.Fields{
			"object": reflect.TypeOf(repo).String(),
			"method": "FindById",
		}).Debug("Cache miss. Retrieving from database.")
		u = new(models.User)
		row := repo.getStmt.QueryRow(id)

		err = row.Scan(&u.ID,
			&u.Username,
			&u.Password,
			&u.Email,
			&u.EmailConfirmed,
			&u.Role,
			&u.Locked,
			&u.TwoFactorEnabled,
			&u.Registered,
			&u.AccessFailedCount)

		if err != nil {
			return nil, err
		}
		logrus.WithFields(logrus.Fields{
			"object": reflect.TypeOf(repo).String(),
			"method": "FindById",
		}).Debug("Success")

		logrus.WithFields(logrus.Fields{
			"object": reflect.TypeOf(repo).String(),
			"method": "FindById",
		}).Debug("Adding user to cache")
		repo.cache(*u)
	}

	return u, nil
}

// FindByEmail tries to retrieve a User from database
func (repo *UserRepoSQL) FindByEmail(id string) (*models.User, error) {
	logrus.WithFields(logrus.Fields{
		"object": reflect.TypeOf(repo).String(),
		"method": "FindByEmail",
		"id":     id,
	}).Debug("Retrieving user from cache")
	u, err := repo.retrieveFromCache(id)
	if err == redis.Nil || err == berrors.BioCacheDisabled {
		logrus.WithFields(logrus.Fields{
			"object": reflect.TypeOf(repo).String(),
			"method": "FindByEmail",
		}).Debug("Cache miss. Retrieving from database.")
		u = new(models.User)
		row := repo.getEmailStmt.QueryRow(id)

		err = row.Scan(&u.ID,
			&u.Username,
			&u.Password,
			&u.Email,
			&u.EmailConfirmed,
			&u.Role,
			&u.Locked,
			&u.TwoFactorEnabled,
			&u.Registered,
			&u.AccessFailedCount)

		if err != nil {
			return nil, errors.Wrapf(err, "failed to find user by email: %v", err.Error())
		}
		logrus.WithFields(logrus.Fields{
			"object": reflect.TypeOf(repo).String(),
			"method": "FindById",
		}).Debug("Success")

		logrus.WithFields(logrus.Fields{
			"object": reflect.TypeOf(repo).String(),
			"method": "FindById",
		}).Debug("Adding user to cache")
		repo.cache(*u)
	}

	return u, nil
}

func (repo *UserRepoSQL) Find(query string) ([]*models.User, error) {
	users := make([]*models.User, 0)

	rows, err := repo.findStmt.Query(query)
	if err != nil {
		return nil, errors.Wrapf(err, "failed to find user: %v", err.Error())
	}

	for rows.Next() {
		u := new(models.User)

		err := rows.Scan(&u.ID,
			&u.Username,
			&u.Password,
			&u.Email,
			&u.EmailConfirmed,
			&u.Role,
			&u.Locked,
			&u.TwoFactorEnabled,
			&u.Registered,
			&u.AccessFailedCount)

		if err != nil {
			return nil, err
		}

		users = append(users, u)
	}
	return users, nil
}

func (repo *UserRepoSQL) Delete(id string) error {
	_, err := repo.deleteStmt.Exec(id)

	if err != nil {
		return err
	}

	repo.invalidateCache(id)

	return nil
}

func (repo *UserRepoSQL) retrieveFromCache(id string) (*models.User, error) {
	redisID := fmt.Sprintf("entity:user:id:%v", id)
	cacheEmail := fmt.Sprintf("entity:user:email:%v", id)

	res, err := repo._cache.Get(redisID)

	if err == cache.Nil || len(res) == 0 {
		logrus.WithField("redis_id", redisID).Debug("Cache miss")
		res, err = repo._cache.Get(cacheEmail)
		if err == nil {
			goto found
		}
		return nil, redis.Nil
	} else if err != nil {
		panic(err)
	}

found:

	user := new(models.User)

	err = json.Unmarshal([]byte(res), &user)
	if err != nil {
		panic(err)
	}
	return user, nil
}

// cache attempts to store user in Redis cache
func (repo *UserRepoSQL) cache(u models.User) error {
	redisID := fmt.Sprintf("entity:user:id:%v", u.ID)
	cacheEmail := fmt.Sprintf("entity:user:email:%v", u.Email)

	repo._cache.Set(redisID, u)
	repo._cache.Set(cacheEmail, u)

	return nil
}

func (repo *UserRepoSQL) invalidateCache(id string) {

	redisID := fmt.Sprintf("entity:user:id:%v", id)

	err := repo._cache.Delete(redisID)
	if err != nil && err != cache.Nil {
		logrus.WithFields(logrus.Fields{
			"redis_id":    redisID,
			"redis_error": err.Error(),
		}).Error("Failed to delete cache entry")
	}
}
