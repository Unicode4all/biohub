package primitives

import (
	"errors"
	"reflect"
	"sync"
)

var (
	// ServiceCollection is a package-wide service locator
	ServiceCollection *ServiceLocator
)

// ServiceLocator is a simple service locator that uses reflection
type ServiceLocator struct {
	services []interface{}
	types    []reflect.Type
	values   []reflect.Value

	lock sync.Mutex
}

// AddService stores an object to be accessible via GetService
func (sloc *ServiceLocator) AddService(service interface{}) error {
	sloc.lock.Lock()
	defer sloc.lock.Unlock()
	err := sloc.checkService(service)

	if err != nil {
		return err
	}

	sloc.services = append(sloc.services, service)
	sloc.types = append(sloc.types, reflect.TypeOf(service))
	sloc.values = append(sloc.values, reflect.ValueOf(service))

	return nil
}

// GetService attempts to retrieve stored service and set the argument to service's pointer
func (sloc *ServiceLocator) GetService(service interface{}) error {
	sloc.lock.Lock()
	defer sloc.lock.Unlock()

	k := reflect.TypeOf(service).Elem()
	kind := k.Kind()
	if kind == reflect.Ptr {
		k = k.Elem()
		kind = k.Kind()
	}

	// Iterate through stored types
	for i, t := range sloc.types {
		// If service is a interface that implements stored object set it to the pointer of that object
		if kind == reflect.Interface && t.Implements(k) {
			reflect.Indirect(
				reflect.ValueOf(service),
			).Set(sloc.values[i])
			return nil
		} else if kind == reflect.Struct && k.AssignableTo(t.Elem()) {
			reflect.ValueOf(service).Elem().Set(sloc.values[i])
			return nil
		}
	}
	return errors.New("Can't retrieve requested service")
}

// GetService attempts to retrieve stored service and set the argument to service's pointer
func (sloc *ServiceLocator) checkService(service interface{}) error {
	k := reflect.TypeOf(service).Elem()
	kind := k.Kind()
	if kind == reflect.Ptr {
		k = k.Elem()
		kind = k.Kind()
	}

	// Iterate through stored types
	for _, t := range sloc.types {
		// If service is a interface that implements stored object set it to the pointer of that object
		if kind == reflect.Interface && t.Implements(k) {
			return errors.New("Service already exists")
		} else if kind == reflect.Struct && k.AssignableTo(t.Elem()) {
			return errors.New("Service already exists")
		}
	}
	return nil
}
