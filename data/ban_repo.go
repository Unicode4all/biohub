package data

import (
	"github.com/Unicode4all/biohub/models"
)

type BanRepo interface {
	AddOrUpdate(newUser models.Ban) error
	FindById(id string) (models.Ban, error)
	Find(query string) []models.Ban
	GetAll(userid string) []models.Ban
	Remove(ban string) error
}
