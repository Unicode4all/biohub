package user

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/Unicode4all/biohub/helpers"
	"github.com/Unicode4all/biohub/helpers/berrors"
	"github.com/Unicode4all/biohub/models/reqmodels"
	"github.com/Unicode4all/biohub/services"
	"github.com/Unicode4all/biohub/services/primitives"
	"github.com/go-chi/chi"
	"github.com/go-playground/validator/v10"
	"github.com/sirupsen/logrus"
)

func Register(w http.ResponseWriter, r *http.Request) {
	b, err := ioutil.ReadAll(r.Body)
	if err != nil {
		helpers.APIError(w, r, http.StatusInternalServerError, err)
		return
	}

	var u reqmodels.User
	err = json.Unmarshal(b, &u)
	if err != nil {
		helpers.APIError(w, r, http.StatusBadRequest, err)
		return
	}

	validate := validator.New()

	err = validate.Struct(u)
	if err != nil {
		if _, ok := err.(*validator.InvalidValidationError); ok {
			helpers.APIError(w, r, http.StatusInternalServerError, err)
			return
		}

		for _, err := range err.(validator.ValidationErrors) {
			logrus.WithFields(logrus.Fields{
				"namespace":        err.Namespace(),
				"field":            err.Field(),
				"struct_namespace": err.StructNamespace(),
				"struct_field":     err.StructField(),
				"tag":              err.Tag(),
			}).Debug("Validation error")
		}

		helpers.APIError(w, r, http.StatusBadRequest, err)
		return
	}

	var userManager *services.UserManager
	err = primitives.ServiceCollection.GetService(&userManager)
	if err != nil {
		helpers.APIError(w, r, http.StatusInternalServerError, err)
		return
	}

	_, err = userManager.Register(u)

	if err != nil {
		if dup, ok := err.(*berrors.BioDuplicateEntry); ok {
			helpers.APIError(w, r, http.StatusBadRequest, fmt.Errorf("An user with same %v already exist", dup.Constraint))
			return
		}
		helpers.APIError(w, r, http.StatusInternalServerError, err)
		return
	}

	w.WriteHeader(200)
	return
}

func Login(w http.ResponseWriter, r *http.Request) {
	b, err := ioutil.ReadAll(r.Body)
	if err != nil {
		helpers.APIError(w, r, http.StatusInternalServerError, err)
		return
	}

	var u reqmodels.Login
	err = json.Unmarshal(b, &u)
	if err != nil {
		helpers.APIError(w, r, http.StatusBadRequest, err)
		return
	}

	var userManager *services.UserManager
	err = primitives.ServiceCollection.GetService(&userManager)
	if err != nil {
		panic(err)
	}

	err = userManager.SignIn(u, r, w)
	if err != nil {
		helpers.APIError(w, r, http.StatusUnauthorized, err)
	}

	w.WriteHeader(200)
}

func NewAPI() http.Handler {
	router := chi.NewRouter()

	router.Post("/register", func(w http.ResponseWriter, r *http.Request) {
		Register(w, r)
	})
	router.Post("/login", func(w http.ResponseWriter, r *http.Request) {
		Login(w, r)
	})

	return router
}
