package cache

import (
	"fmt"
	"time"

	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

var provides = make(map[string]CacheProvider)

type CacheProvider interface {
	Init() error
	Set(key string, value interface{})
	SetString(key string, value string)
	Get(key string) (string, error)
	HSet(key string, hkey string, value interface{})
	HGet(key string, hkey string) (string, error)

	StringMapSet(key string, value map[string]interface{}) error
	StringMapGet(key string) (map[string]string, error)

	StringMapSetField(key string, field string, value interface{}) error
	StringMapGetField(key string, field string) (string, error)

	StringMapDelField(key string, field string) error

	Expire(key string, duration time.Duration) error

	Delete(key string) error
}

type Cache struct {
	provider CacheProvider
}

func RegisterCacheProvider(name string, provider CacheProvider) {
	if provider == nil {
		panic("cache: Register provider is nil")
	}

	if _, dup := provides[name]; dup {
		panic("cache: Register called twice for provider " + name)
	}
	provides[name] = provider
}

func NewCache(providerName string) (*Cache, error) {
	if viper.GetBool("Caching.Enabled") == false {
		providerName = "memory"
		logrus.New().Warnf("Cache: caching is disabled. Will fall back to memory cache")
	}

	provider, ok := provides[providerName]
	if !ok {
		return nil, fmt.Errorf("Cache: Provider %v is not supported", providerName)
	}

	err := provider.Init()
	if err != nil {
		return nil, errors.Wrapf(err, "Cache: unable to create cahe: %v", err.Error())
	}

	return &Cache{provider: provider}, nil
}

func (c *Cache) SetString(key string, value string) {
	c.provider.Set(key, value)
}

func (c *Cache) Set(key string, value interface{}) {
	c.provider.Set(key, value)
}

func (c *Cache) Get(key string) (string, error) {
	m, err := c.provider.Get(key)
	return m, err
}

func (c *Cache) Delete(key string) error {
	return c.provider.Delete(key)
}

func (c *Cache) HSet(key string, hkey string, value interface{}) {
	c.provider.HSet(key, hkey, value)
}
func (c *Cache) HGet(key string, hkey string) (string, error) {
	return c.provider.HGet(key, hkey)
}

func (c *Cache) StringMapSet(key string, value map[string]interface{}) error {
	return c.provider.StringMapSet(key, value)
}
func (c *Cache) StringMapGet(key string) (map[string]string, error) {
	return c.provider.StringMapGet(key)
}

func (c *Cache) StringMapSetField(key string, field string, value interface{}) error {
	return c.provider.StringMapSetField(key, field, value)
}
func (c *Cache) StringMapGetField(key string, field string) (string, error) {
	return c.provider.StringMapGetField(key, field)
}

func (c *Cache) StringMapDelField(key string, field string) error {
	return c.provider.StringMapDelField(key, field)
}
