module github.com/Unicode4all/biohub

go 1.13

require (
	github.com/360EntSecGroup-Skylar/goreporter v0.0.0-20180902115603-df1b20f7c5d0 // indirect
	github.com/fsnotify/fsnotify v1.4.7
	github.com/go-chi/chi v4.0.2+incompatible
	github.com/go-chi/render v1.0.1
	github.com/go-errors/errors v1.0.1
	github.com/go-playground/validator/v10 v10.1.0
	github.com/go-redis/redis v6.15.6+incompatible
	github.com/go-redis/redis/v7 v7.0.0-beta.4 // indirect
	github.com/golang-migrate/migrate v3.5.4+incompatible
	github.com/golang-migrate/migrate/v4 v4.7.1
	github.com/google/uuid v1.1.1
	github.com/lib/pq v1.3.0
	github.com/mitchellh/mapstructure v1.1.2
	github.com/patrickmn/go-cache v2.1.0+incompatible
	github.com/pkg/errors v0.8.1
	github.com/sirupsen/logrus v1.4.2
	github.com/spf13/cobra v0.0.5
	github.com/spf13/viper v1.3.2
	golang.org/x/crypto v0.0.0-20190426145343-a29dc8fdc734
	mvdan.cc/lint v0.0.0-20170908181259-adc824a0674b // indirect
)
