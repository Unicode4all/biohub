package models

type ErrorDTO struct {
	Code      int
	Error     string
	RequestID string
}
