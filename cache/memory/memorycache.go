package cmem

import (
	"encoding/json"
	"time"

	"github.com/Unicode4all/biohub/cache"
	gocache "github.com/patrickmn/go-cache"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

var pder = &MemCacheProvider{}

func init() {
	cache.RegisterCacheProvider("memory", pder)
}

type MemCacheProvider struct {
	memcache *gocache.Cache
}

func (c *MemCacheProvider) Init() error {
	c.memcache = gocache.New(viper.GetDuration("Caching.Duration"), 10*time.Minute)
	if viper.GetBool("Caching.Enabled") {
		logrus.Info("Cache: using in-memory cache")
	} else {
		logrus.Info("Cache: memory cache will only be used for session storage")
	}

	return nil
}

func (c *MemCacheProvider) SetString(key string, value string) {
	c.memcache.Set(key, value, gocache.DefaultExpiration)
}

func (c *MemCacheProvider) Set(key string, value interface{}) {
	json, err := json.Marshal(value)
	if err != nil {
		panic(err)
	}

	c.memcache.Set(key, json, gocache.DefaultExpiration)
}

func (c *MemCacheProvider) HSet(key string, hkey string, value interface{}) {
	c.memcache.Set(key+":"+hkey, value, gocache.DefaultExpiration)
}

func (c *MemCacheProvider) HGet(key string, hkey string) (string, error) {
	m, found := c.memcache.Get(key + ":" + hkey)
	if !found {
		return "", cache.Nil
	}
	return m.(string), nil
}

func (c *MemCacheProvider) StringMapSet(key string, value map[string]interface{}) error {
	c.memcache.Set(key, value, gocache.DefaultExpiration)
	return nil
}

func (c *MemCacheProvider) StringMapGet(key string) (map[string]string, error) {
	r, found := c.memcache.Get(key)
	if !found {
		return nil, cache.Nil
	}

	r, ok := r.(map[string]string)
	if !ok {
		return nil, cache.WrongType
	}

	return r.(map[string]string), nil
}

func (c *MemCacheProvider) StringMapSetField(key string, field string, value interface{}) error {
	r, found := c.memcache.Get(key)
	if !found {
		return cache.Nil
	}

	r, ok := r.(map[string]interface{})
	if !ok {
		return cache.WrongType
	}

	r.(map[string]interface{})[field] = value

	c.memcache.Set(key, r, gocache.DefaultExpiration)
	return nil
}

func (c *MemCacheProvider) StringMapGetField(key string, field string) (string, error) {
	r, found := c.memcache.Get(key)
	if !found {
		return "", cache.Nil
	}

	r, ok := r.(map[string]string)
	if !ok {
		return "", cache.WrongType
	}

	return r.(map[string]string)[field], nil
}

func (c *MemCacheProvider) Expire(key string, duration time.Duration) error {
	m, found := c.memcache.Get(key)
	if !found {
		return cache.Nil
	}

	c.memcache.Set(key, m, duration)
	return nil
}

func (c *MemCacheProvider) StringMapDelField(key string, field string) error {
	r, found := c.memcache.Get(key)
	if !found {
		return cache.Nil
	}

	r, ok := r.(map[string]string)
	if !ok {
		return cache.WrongType
	}

	delete(r.(map[string]string), field)

	c.StringMapSet(key, r.(map[string]interface{}))
	return nil

}

func (c *MemCacheProvider) Get(key string) (string, error) {
	m, found := c.memcache.Get(key)
	if !found {
		return "", cache.Nil
	}
	return m.(string), nil
}

func (c *MemCacheProvider) Delete(key string) error {
	c.memcache.Delete(key)
	return nil
}
