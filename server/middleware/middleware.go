package middleware

import (
	"net/http"
	"time"

	"github.com/Unicode4all/biohub/security"
	"github.com/Unicode4all/biohub/services/primitives"
	"github.com/sirupsen/logrus"

	cmiddleware "github.com/go-chi/chi/middleware"
)

func Logger(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ww := cmiddleware.NewWrapResponseWriter(w, r.ProtoMajor)
		t1 := time.Now()
		defer func() {
			logrus.WithFields(logrus.Fields{
				"request_id":    cmiddleware.GetReqID(r.Context()),
				"method":        r.Method,
				"status":        ww.Status(),
				"bytes_written": ww.BytesWritten(),
				"completed_in":  time.Since(t1),
				"uri":           r.URL,
			}).Info("HTTP Request")
		}()
		next.ServeHTTP(ww, r)
	})
}

func Authenticate(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		_, err := r.Cookie("biosession")
		if err != nil {
			if err == http.ErrNoCookie {
				w.WriteHeader(http.StatusUnauthorized)
				return
			}

			w.WriteHeader(http.StatusBadRequest)
			return
		}

		//sessionToken := c.Value

		var sessionManager *security.SessionManager
		err = primitives.ServiceCollection.GetService(&sessionManager)
		if err != nil {
			panic(err)
		}

		session := sessionManager.StartSession(w, r)
		if session.SessionID() == "" {
			w.WriteHeader(http.StatusUnauthorized)
			return
		}
		next.ServeHTTP(w, r)
	})
}
