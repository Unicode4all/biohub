package helpers

import (
	"encoding/json"
	"net/http"

	"github.com/Unicode4all/biohub/models"
	"github.com/go-chi/chi/middleware"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
)

type stackTracer interface {
	StackTrace() errors.StackTrace
}

func APIError(w http.ResponseWriter, r *http.Request, code int, err error) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)

	if code == http.StatusInternalServerError {
		if errstack, ok := err.(stackTracer); ok {
			logrus.
				WithField("request_id", middleware.GetReqID(r.Context())).
				Errorf("An error has been occured while processing request: %v\nInner error: %v\nStack trace:%+v", err.Error(), errors.Cause(err).Error(), errstack.StackTrace()[0:5])
		}
	}

	js, _ := json.Marshal(models.ErrorDTO{
		Code:      code,
		Error:     err.Error(),
		RequestID: middleware.GetReqID(r.Context()),
	})
	w.Write(js)
}
