package data

import "database/sql"

import "github.com/go-redis/redis"

var (
	DBConn *sql.DB
	Redis  *redis.Client
)
