package security

import (
	"fmt"
	"time"

	"github.com/Unicode4all/biohub/cache"
	"github.com/Unicode4all/biohub/data"
	"github.com/go-redis/redis"
	"github.com/pkg/errors"
)

type Session struct {
	Sid          string
	timeAccessed time.Time
	_cache       *cache.Cache
	_manager     *SessionManager //back reference to the manager
	value        map[string]interface{}
}

// Set stores a value in session stored in Redis cache
func (st *Session) Set(key, value interface{}) error {
	err := st._cache.StringMapSetField(st.Sid, fmt.Sprintf("%v", key), value)
	if err != nil {
		return errors.Wrap(err, "session: unable to set session value")
	}
	st._manager.UpdateSession(st.Sid)
	return nil
}

func (st *Session) PassClient(c interface{}) {
	if tmp, ok := c.(*redis.Client); ok {
		data.Redis = tmp
	}
}

// Get tries to get value from session stored in Redis cache
func (st *Session) Get(key interface{}) interface{} {
	st._manager.UpdateSession(st.Sid)
	v, err := st._cache.StringMapGetField(st.Sid, fmt.Sprintf("%v", key))
	if err == redis.Nil {
		return nil
	} else if err != nil {
		panic(err)
	}

	return v
}

// Delete removes value from session stored in Redis cache
func (st *Session) Delete(key interface{}) error {
	err := st._cache.StringMapDelField(st.Sid, fmt.Sprintf("%v", key))

	if err == cache.Nil {
		return err
	} else if err != nil {
		panic(err)
	}

	st._manager.UpdateSession(st.Sid)
	return nil
}

// SessionID returns current session id
func (st *Session) SessionID() string {
	return st.Sid
}
