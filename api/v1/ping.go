package v1

import (
	"net/http"

	"github.com/Unicode4all/biohub/server/middleware"
	"github.com/go-chi/chi"
)

func Ping(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
}

func NewPingAPI() http.Handler {
	router := chi.NewRouter()

	router.Use(middleware.Authenticate)
	router.Get("/ping", Ping)

	return router
}
