package security

import (
	"fmt"
	"net/http"
	"net/url"
	"sync"
	"time"

	"github.com/Unicode4all/biohub/cache"
	"github.com/Unicode4all/biohub/services/primitives"
	"github.com/mitchellh/mapstructure"
	"github.com/pkg/errors"
)

type SessionManager struct {
	cookieName  string
	lock        sync.Mutex
	maxlifetime int64
	_cache      *cache.Cache
}

func NewSessionManager(cookieName string,
	maxlifetime int64) (*SessionManager, error) {
	var c *cache.Cache
	err := primitives.ServiceCollection.GetService(&c)
	if err != nil {
		return nil, errors.Wrap(err, "session: failed to create session manager")
	}
	return &SessionManager{cookieName: cookieName, maxlifetime: maxlifetime, _cache: c}, nil
}

func (pder *SessionManager) InitSession(sid string) (Session, error) {
	v := map[string]interface{}{}
	newsess := Session{Sid: sid, timeAccessed: time.Now(), _cache: pder._cache, _manager: pder}
	mapstructure.Decode(newsess, &v)
	err := pder._cache.StringMapSet(sid, v)
	if err != nil {
		return Session{}, errors.Wrap(err, "session: failed to init session")
	}

	newsess.value = v

	return newsess, nil
}

func (manager *SessionManager) StartSession(w http.ResponseWriter,
	r *http.Request) (session Session) {
	manager.lock.Lock()
	defer manager.lock.Unlock()
	cookie, err := r.Cookie(manager.cookieName)
	if err != nil || cookie.Value == "" {
		sid, _ := GenerateRandomStringURLSafe(64)
		session, _ = manager.InitSession(sid)
		cookie := http.Cookie{Name: manager.cookieName, Value: url.QueryEscape(sid), Path: "/", HttpOnly: true, MaxAge: int(manager.maxlifetime / 1000000000)}
		http.SetCookie(w, &cookie)
	} else {
		sid, _ := url.QueryUnescape(cookie.Value)
		session, _ = manager.ReadSession(sid)
	}
	return
}

func (pder *SessionManager) ReadSession(sid string) (Session, error) {

	m, err := pder._cache.StringMapGet(sid)
	if err == cache.Nil {
		sess, err := pder.InitSession(sid)
		return sess, errors.Wrap(err, "session: failed to read session")
	} else if err != nil {
		panic(errors.Wrap(err, "session: failed to read session"))
	}

	v := make(map[string]interface{})

	for key, value := range m {
		strKey := fmt.Sprintf("%v", key)
		strValue := fmt.Sprintf("%v", value)

		v[strKey] = strValue
	}

	sess := Session{
		Sid:          sid,
		_cache:       pder._cache,
		timeAccessed: time.Now(),
		value:        v,
	}

	return sess, nil
}

func (manager *SessionManager) UpdateSession(sid string) error {
	return nil
}

func (manager *SessionManager) DestroySession(w http.ResponseWriter, r *http.Request) {
	cookie, err := r.Cookie(manager.cookieName)
	if err != nil || cookie.Value == "" {
		return
	} else {
		manager.lock.Lock()
		defer manager.lock.Unlock()
		manager._cache.Delete(cookie.Value)
		expiration := time.Now()
		cookie := http.Cookie{Name: manager.cookieName, Path: "/", HttpOnly: true, Expires: expiration, MaxAge: -1}
		http.SetCookie(w, &cookie)
	}
}
