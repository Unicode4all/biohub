package data_test

import (
	"database/sql"
	"reflect"
	"testing"

	"github.com/Unicode4all/biohub/data"
	_ "github.com/Unicode4all/biohub/data"
	"github.com/Unicode4all/biohub/models"
	"github.com/go-redis/redis"
)

func TestNewUserRepoSQL(t *testing.T) {
	type args struct {
		db    *sql.DB
		redis *redis.Client
	}
	tests := []struct {
		name    string
		args    args
		want    *data.UserRepoSQL
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := data.NewUserRepoSQL(tt.args.db, tt.args.redis)
			if (err != nil) != tt.wantErr {
				t.Errorf("NewUserRepoSQL() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewUserRepoSQL() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUserRepoSQL_AddOrUpdate(t *testing.T) {
	type args struct {
		user models.User
	}
	tests := []struct {
		name    string
		repo    *data.UserRepoSQL
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := tt.repo.AddOrUpdate(tt.args.user); (err != nil) != tt.wantErr {
				t.Errorf("UserRepoSQL.AddOrUpdate() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestUserRepoSQL_FindById(t *testing.T) {
	type args struct {
		id string
	}
	tests := []struct {
		name    string
		repo    *data.UserRepoSQL
		args    args
		want    *models.User
		wantErr bool
	}{}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.repo.FindById(tt.args.id)
			if (err != nil) != tt.wantErr {
				t.Errorf("UserRepoSQL.FindById() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("UserRepoSQL.FindById() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUserRepoSQL_Find(t *testing.T) {
	type args struct {
		query string
	}
	tests := []struct {
		name    string
		repo    *data.UserRepoSQL
		args    args
		want    []*models.User
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.repo.Find(tt.args.query)
			if (err != nil) != tt.wantErr {
				t.Errorf("UserRepoSQL.Find() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("UserRepoSQL.Find() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUserRepoSQL_Delete(t *testing.T) {
	type args struct {
		id string
	}
	tests := []struct {
		name    string
		repo    *data.UserRepoSQL
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := tt.repo.Delete(tt.args.id); (err != nil) != tt.wantErr {
				t.Errorf("UserRepoSQL.Delete() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
