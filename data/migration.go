package data

import (
	"database/sql"

	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database"
	_ "github.com/lib/pq"
	"github.com/sirupsen/logrus"
)

func MigrateDB(db *sql.DB, driver database.Driver, force bool, migration int) {

	m, err := migrate.NewWithDatabaseInstance(
		"file://sql",
		"postgres", driver)
	if err != nil {
		logrus.Fatalf("Cannot instantiate migrations: %v", err.Error())
	}

	v, d, err := m.Version()
	if err == migrate.ErrNilVersion {
		logrus.Warnf("Database was not previously migrated. Will apply all migrations.")
	} else if err == migrate.ErrLocked {
		logrus.Error("Database is locked. Unable to run any migrations.")
		return
	} else if err != nil {
		logrus.Fatal("Unknown error while migrating database")
	}

	logrus.WithField("db_dirty", d).Infof("Current migration version: %v", v)

	if force {
		err = m.Force(migration)
	}
	err = m.Up()
	if err == migrate.ErrNoChange {
		logrus.Info("The database is up to date. Nothing to migrate.")
		return
	} else if err != nil {
		logrus.Errorf("Failed to apply migration: %v", err.Error())
		_, dirty, _ := m.Version()
		if dirty {
			logrus.Fatalf("Database was left in dirty state. This probably was caused by a broken migration or broken database. You must fix it manually and then run \"biohub force-migrate %v\"", v)
		}
		return
	}

	if err == nil {
		v, d, _ := m.Version()
		logrus.WithField("db_dirty", d).Infof("Database was successfully migrated to: %v", v)
	}

}
