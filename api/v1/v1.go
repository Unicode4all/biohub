package v1

import (
	"net/http"

	"github.com/Unicode4all/biohub/api/v1/user"
	"github.com/go-chi/chi"
)

func NewAPI() http.Handler {
	router := chi.NewRouter()

	router.Mount("/user", user.NewAPI())
	router.Mount("/pingapi", NewPingAPI())

	return router
}
