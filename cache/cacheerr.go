package cache

import "errors"

type CacheError string

var Nil = errors.New("cache miss")
var WrongType = errors.New("wrong type")
