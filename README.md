![biohub-light-128](https://user-images.githubusercontent.com/4706357/73595469-2df20480-452a-11ea-8455-6804eb7fea60.png)

## The Free (as in freedom) Space Station 13 hub

![AppVeyor](https://img.shields.io/appveyor/ci/Unicode4all/Biohub?logo=appveyor&style=for-the-badge)

### Features:

- Full featured game statistics
- Everything in one place (accounts, characters, preferences)
- Can be used instead of BYOND accounts
- Remote control your servers (change gamemode, ban people)

### Installation

You can install Biohub from either compiled binaries or source code

#### Building from source

#### Prerequisites

To build Biohub you only need [Go](https://golang.org) installed.

#### Building

Issue the following command within cloned dir:

```
go build -o biohub
```

#### Installation and Configuration

##### Prerequisites

TBD