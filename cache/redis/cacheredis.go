package credis

import (
	"encoding/json"
	"time"

	"github.com/Unicode4all/biohub/cache"
	"github.com/go-redis/redis"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

var pder = &RedisCacheProvider{}

func init() {
	cache.RegisterCacheProvider("redis", pder)
}

type RedisCacheProvider struct {
	redisc *redis.Client
}

func (c *RedisCacheProvider) Init() error {
	redisc := redis.NewClient(&redis.Options{
		Addr:     viper.GetString("Caching.Host"),
		Password: viper.GetString("Caching.Password"),
		DB:       viper.GetInt("Caching.Database"),
	})

	_, err := redisc.Ping().Result()
	if err != nil {
		return errors.Wrapf(err, "Redis: can't initialize redis cache %v", err.Error())
	}

	logrus.Info("Redis: Successfully connected to Redis")
	return nil
}

func (c *RedisCacheProvider) SetString(key string, value string) {
	err := c.redisc.Set(key, value, viper.GetDuration("Cache.Duration")).Err()
	if err != nil {
		panic(err)
	}
}

func (c *RedisCacheProvider) Set(key string, value interface{}) {
	json, err := json.Marshal(value)
	if err != nil {
		panic(err)
	}

	err = c.redisc.Set(key, json, viper.GetDuration("Cache.Duration")).Err()
	if err != nil {
		panic(err)
	}
}

func (c *RedisCacheProvider) Get(key string) (string, error) {
	m := c.redisc.Get(key)
	if m.Err() == redis.Nil {
		return "", cache.Nil
	}

	return m.Val(), nil
}

func (c *RedisCacheProvider) HSet(key string, hkey string, value interface{}) {
	c.redisc.HSet(key, hkey, value)
}

func (c *RedisCacheProvider) HGet(key string, hkey string) (string, error) {
	m := c.redisc.HGet(key, hkey)
	if m.Err() == redis.Nil {
		return "", cache.Nil
	}
	return m.Val(), nil
}

func (c *RedisCacheProvider) StringMapSet(key string, value map[string]interface{}) error {
	err := c.redisc.HMSet(key, value).Err()
	if err != nil {
		return err
	}
	return nil
}

func (c *RedisCacheProvider) StringMapGet(key string) (map[string]string, error) {
	r := c.redisc.HGetAll(key)
	if r.Err() == redis.Nil {
		return nil, cache.Nil
	} else if r.Err() != nil {
		return nil, r.Err()
	}

	return r.Val(), nil
}

func (c *RedisCacheProvider) StringMapSetField(key string, field string, value interface{}) error {
	err := c.redisc.HSet(key, field, viper.GetDuration("Security.SessionDuration")).Err()
	if err == redis.Nil {
		return cache.Nil
	} else if err != nil {
		return err
	}

	return nil
}

func (c *RedisCacheProvider) StringMapGetField(key string, field string) (string, error) {
	result := c.redisc.HGet(key, field)
	if result.Err() == redis.Nil {
		return "", cache.Nil
	} else if result.Err() != nil {
		return "", result.Err()
	}

	return result.Val(), nil
}

func (c *RedisCacheProvider) StringMapDelField(key string, field string) error {
	result := c.redisc.HDel(key, field)
	if result.Err() == redis.Nil {
		return cache.Nil
	} else if result.Err() != nil {
		return result.Err()
	}

	return nil

}

func (c *RedisCacheProvider) Expire(key string, duration time.Duration) error {
	err := c.redisc.Expire(key, duration).Err()
	if err != redis.Nil {
		return cache.Nil
	} else if err != nil {
		return errors.Wrap(err, "cache: cannot set expiration time for cached item")
	}

	return nil
}

func (c *RedisCacheProvider) Delete(key string) error {
	err := c.redisc.Del(key).Err()
	if err != nil {
		return err
	}
	return nil
}
