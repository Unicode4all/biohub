package cmd

import (
	"os"
	"os/signal"
	"syscall"

	"github.com/fsnotify/fsnotify"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"

	"github.com/Unicode4all/biohub/server"
)

var rootCmd = &cobra.Command{
	Use:   "biohub",
	Short: "Biohub is a powerful Space Station 13 server management platform",

	Run: func(cmd *cobra.Command, args []string) {
		sc := make(chan os.Signal, 1)
		signal.Notify(sc, os.Interrupt, syscall.SIGTERM, syscall.SIGHUP)
		go server.Startup()

		s := <-sc // Block current thread until any signal is received

		switch s {
		case os.Interrupt:
			logrus.Info("Terminate signal received. Stopping server.")
			break
		}
	},
}

//Execute will be executed from main
func Execute() error {
	return rootCmd.Execute()
}

func init() {
	logrus.SetFormatter(&logrus.TextFormatter{
		DisableColors: false,
		FullTimestamp: true,
		ForceColors:   true,
	})

	rootCmd.AddCommand(forceMigrateCmd)

	cobra.OnInitialize(initConfig)
}

func initConfig() {

	viper.SetDefault("General.Listen", "localhost:8080")
	viper.SetDefault("Database.ConnectionString", "user=postgres password=password host=localhost port=5432 database=biohub")

	viper.SetConfigName("biohub")

	viper.AddConfigPath(".")
	viper.AddConfigPath("/etc/")
	viper.AddConfigPath("$HOME/.config/")

	if err := viper.ReadInConfig(); err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); ok {
			logrus.Error("No configruration files has been found in any of search paths")
		} else {
			logrus.Fatalf("Could not read config: %v", err)
		}
	}

	viper.WatchConfig()
	viper.OnConfigChange(func(e fsnotify.Event) {
		logrus.WithField("config_file", e.Name).Info("Config file changed, applying changes")
	})

}
