package models

type Notice struct {
	ID      string `json:"id"`
	Message string `json:"message"`
	Admin   User   `json:"admin"`
}
