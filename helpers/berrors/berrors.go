package berrors

// BioError is sent when cache is disabled
type BioError string

func (e BioError) Error() string { return string(e) }

const BioCacheDisabled = BioError("redis: cache disabled")

const BioServiceAlreadyExists = BioError("servicelocator: service already exists")

func NewBioDuplicateEntry(constr string) error {
	return &BioDuplicateEntry{constr}
}

type BioDuplicateEntry struct {
	Constraint string
}

func (bde *BioDuplicateEntry) Error() string {
	return "database: an entry (or entries) with same value detected"
}
