package server

import (
	"crypto/tls"
	"net/http"
	"time"

	"github.com/go-redis/redis"
	"github.com/golang-migrate/migrate/v4/database/postgres"

	"github.com/sirupsen/logrus"

	"database/sql"

	"github.com/Unicode4all/biohub/cache"
	"github.com/Unicode4all/biohub/data"
	"github.com/Unicode4all/biohub/security"
	"github.com/Unicode4all/biohub/services"
	"github.com/Unicode4all/biohub/services/primitives"
	_ "github.com/lib/pq"
	"github.com/spf13/viper"

	_ "github.com/golang-migrate/migrate/v4/source/file"

	_ "github.com/Unicode4all/biohub/cache/memory"
	_ "github.com/Unicode4all/biohub/cache/redis"
)

var RedisClient *redis.Client

// Startup sets up http server and register all necessary services
func Startup() {

	loglevel := viper.GetString("Logging.Level")

	switch loglevel {
	case "debug":
		logrus.SetLevel(logrus.DebugLevel)
	case "default":
		logrus.SetLevel(logrus.InfoLevel)
	case "error":
		logrus.SetLevel(logrus.ErrorLevel)
	}

	db, err := sql.Open("postgres", viper.GetString("Database.ConnectionString"))

	if err != nil {
		logrus.WithField("error_message", err.Error()).Fatal("Unable to connect to the database")
	} else {
		logrus.Info("Successfully connected to the database")
	}

	driver, err := postgres.WithInstance(db, &postgres.Config{})
	if err != nil {
		logrus.Errorf("Failed to create PostgreSQL driver instance: %v", err.Error())
		return
	}

	data.MigrateDB(db, driver, false, 0)

	primitives.ServiceCollection = new(primitives.ServiceLocator)

	var userRepo data.UserRepo

	var redisc *redis.Client = nil

	cache, err := cache.NewCache(viper.GetString("Caching.Provider"))
	if err != nil {
		logrus.Fatalf("Cannot create cache: %v", err)
	}

	primitives.ServiceCollection.AddService(cache)

	userRepo, err = data.NewUserRepoSQL(db, redisc)
	if err != nil {
		logrus.WithField("error", err.Error()).Fatal("Could not initialize User repository")
	}

	data.Redis = redisc
	data.DBConn = db

	primitives.ServiceCollection.AddService(userRepo)

	userManager, err := services.NewUserManager()
	if err != nil {
		logrus.Fatalf("Cannot init user manager: %v", err.Error())
	}

	sessionManager, err := security.NewSessionManager(viper.GetString("Security.CookieName"),
		viper.GetInt64("Security.SessionDuration")*int64(time.Minute))
	if err != nil {
		logrus.Fatalf("Cannot initialize session manager: %v", err.Error())
	}

	primitives.ServiceCollection.AddService(userManager)
	primitives.ServiceCollection.AddService(sessionManager)

	baseRouter := NewBaseRouter()

	tc := &tls.Config{
		MinVersion:               tls.VersionTLS12,
		CurvePreferences:         []tls.CurveID{tls.CurveP521, tls.CurveP384, tls.CurveP256},
		PreferServerCipherSuites: true,
		CipherSuites: []uint16{
			tls.TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384,
			tls.TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,
			tls.TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305,
			tls.TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305,
			tls.TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,
			tls.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
		},
		NextProtos: []string{"h2", "http/1.1"},
	}

	hs := &http.Server{
		Addr:         viper.GetString("General.Listen"),
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 10 * time.Second,
		IdleTimeout:  120 * time.Second,
		TLSConfig:    tc,
		Handler:      baseRouter,
	}

	logrus.WithFields(logrus.Fields{
		"listen_address": viper.GetString("General.Listen"),
	}).Info("Starting server")
	if viper.Get("General.UseTLS") == true {
		err = hs.ListenAndServeTLS(viper.GetString("General.TLSCertificate"), viper.GetString("General.TLSPrivateKey"))
	} else {
		err = hs.ListenAndServe()
	}

	if err != nil {
		logrus.WithFields(logrus.Fields{
			"listen_address": viper.GetString("General.Listen"),
			"error":          err.Error(),
		}).Fatal("Could not start server")
	}
}
