package data

import (
	"database/sql"
	"time"

	"github.com/Unicode4all/biohub/models"
	"github.com/go-redis/redis"
	"github.com/pkg/errors"
)

type BanRepoSql struct {
	_db    *sql.DB
	_redis *redis.Client

	addToUser *sql.Stmt
	addJobs   *sql.Stmt
}

func NewBanRepoSql(db *sql.DB, redis *redis.Client) BanRepo {
	banRepo := new(BanRepoSql)

	banRepo._db = db
	banRepo._redis = redis

	return banRepo
}

func (repo *BanRepoSql) AddOrUpdate(newBan models.Ban) error {
	if newBan.UserId == "" {
		return errors.New("BanRepo: I don't know to which user I should add a ban")
	}

	newBan.Date = time.Now()

	return nil

}

func (repo *BanRepoSql) FindById(id string) (models.Ban, error) {
	var b models.Ban
	return b, nil
}

func (repo *BanRepoSql) Find(query string) []models.Ban {
	return nil
}

func (repo *BanRepoSql) GetAll(userid string) []models.Ban {
	return nil
}

func (repo *BanRepoSql) Remove(ban string) error {
	return nil
}
