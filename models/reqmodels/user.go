package reqmodels

type User struct {
	Email    string `json:"email,omitempty" validate:"required,email"`
	Password string `json:"password,omitempty" validate:"required,min=8"`
	Username string `json:"username,omitempty" validate:"required,min=4"`
}
